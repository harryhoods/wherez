//
//  ViewController.h
//  wherez
//
//  Created by Hariprasad Prabhakaran on 12/18/13.
//  Copyright (c) 2013 Hariprasad Prabhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBookUI/AddressBookUI.h>
#import <Mapkit/Mapkit.h>
#import "HiddenViewController.h"

@interface ViewController : UIViewController <CLLocationManagerDelegate>{
	CLLocationManager *locationManager;
    CLLocation* location;
    NSDate* eventDate;
    
    UIStoryboard *storyboard;
    HiddenViewController *hiddenVC;
    
    NSString *contact; // Contact detail picked up from the device ContactList
}
@property (weak, nonatomic) IBOutlet UILabel *debugContact;
@property (weak, nonatomic) IBOutlet MKMapView *wherezMap;
@property (weak, nonatomic) IBOutlet UIButton *ShowButton;
@property (weak, nonatomic) IBOutlet UIButton *wherez;
- (IBAction)showCurl:(id)sender;
- (IBAction)lookupContact:(id)sender;

- (void)updateDebugLabel:(id)sender;
@end
