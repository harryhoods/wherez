//
//  HiddenViewController.h
//  wherez
//
//  Created by Hariprasad Prabhakaran on 3/10/14.
//  Copyright (c) 2014 Hariprasad Prabhakaran. All rights reserved.

#import <UIKit/UIKit.h>

//@interface HiddenViewController : UIViewController

@interface HiddenViewController : UITableViewController{
    NSString *finalUserChoice;
    float latitude,longitude;
    NSString *name; //Name of Business
    NSString *subThoroughfare; //street #
    NSString *thoroughfare; //street Name
    NSString *locality; //City
    NSString *administrativeArea; //State
    NSString *postalCode; //Country
    NSString *country; //Country
    NSString *subAdministrativeArea; //County
    NSString *subLocality; //Group name like Bay Area, Puget Sound
}
//@property (nonatomic) id<SettingDelegate> delegate;
-(void)setLatitude:(float)la andLongitude:(float)lo;
-(void)setName:(NSString*)str;
-(void)setStreetNo:(NSString*)str;
-(void)setStreetName:(NSString*)str;
-(void)setCity:(NSString*)str;
-(void)setState:(NSString*)str;
-(void)setZip:(NSString*)str;
-(void)setCountry:(NSString*)str;
-(void)setCounty:(NSString*)str;
-(void)setLocality:(NSString*)str;

@property (weak, nonatomic) IBOutlet UILabel *debugLabel;
@property (weak, nonatomic) IBOutlet UIButton *HideButton;
- (IBAction)dismissView:(id)sender;


@end
