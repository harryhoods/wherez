//
//  AppDelegate.h
//  wherez
//
//  Created by Hariprasad Prabhakaran on 12/18/13.
//  Copyright (c) 2013 Hariprasad Prabhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
