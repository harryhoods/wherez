//
//  ViewController.m
//  wherez
//
//  Created by Hariprasad Prabhakaran on 12/18/13.
//  Copyright (c) 2013 Hariprasad Prabhakaran. All rights reserved.
//

#import "ViewController.h"
#define IS_RETINA ([[UIScreen mainScreen] scale] == 2.0)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IOS_7 (floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_6_1))
#define DEVICE_ORIENTATION ([UIDevice currentDevice].orientation)

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    // defined in the viewWillAppear method - so that Dismissing the hidden Curl view will reload things fine again
}

// Custom Function to position any element passed (like image,buttons,views) relative to the screen size adaptively
- (void)positionElements: (id) sender{
    UIScreen *screen=[UIScreen mainScreen];
    // UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    /******** Orientation Details **************
    //default = 0
    //UIDeviceOrientationPortrait = 1
    //UIDeviceOrientationPortraitUpsideDown = 2
    //UIDeviceOrientationLandscapeLeft = 3
    //UIDeviceOrientationLandscapeRight = 4
    //UIDeviceOrientationUnknown = 5 (flat - screen up)
    *******************************************/
    
    if(sender==Nil)
        sender=self.ShowButton;
    
    // 11 is 1/4th width and height of the button image
    // screen scale is 2.0 for retina display
    float xCenter=11.0*screen.scale;
    float yCenter=11.0*screen.scale;
    if(!IS_IOS_7){
        if(DEVICE_ORIENTATION<=UIDeviceOrientationPortrait){
            yCenter*=2;
            yCenter-=1.5; //nifty Adjustment to ensure the button blends fine
        }
        else{
            xCenter*=2;
            xCenter-=1.5; //nifty Adjustment to ensure the button blends fine
        }
    }
    // Irrespective of orientation, page-turn(settings) button should be in bottom left corner
    // Irrespective of orientation, "Wherez?" button should be in the bottom center
    if(DEVICE_ORIENTATION<=UIDeviceOrientationPortrait){
        [sender setCenter:(CGPointMake(screen.bounds.size.width-xCenter,screen.bounds.size.height-yCenter))];
        [self.wherez setCenter:(CGPointMake(screen.bounds.size.width/2,screen.bounds.size.height-yCenter))];
    }
    else{    //LandscapeLeft, LandscapeRight; also upsideDown - since view stays landscape for upsideDown
        [sender setCenter:(CGPointMake(screen.bounds.size.height-yCenter,screen.bounds.size.width-xCenter))];
        [self.wherez setCenter:(CGPointMake(screen.bounds.size.height/2,screen.bounds.size.width-xCenter))];
    }
    
    // NSLog(@"scale: %f; xcenter[%f]: ycenter[%f]",screen.scale,screen.bounds.size.width-xCenter,screen.bounds.size.height-yCenter);
    // NSLog(@"Version: %f; ios6:%f",floor(NSFoundationVersionNumber),floor(NSFoundationVersionNumber_iOS_6_1));
    // NSLog(@"Orientation:[%d]",DEVICE_ORIENTATION);
}

-(void)orientationChanged:(NSNotification*) Notifier{
    if(DEVICE_ORIENTATION==UIDeviceOrientationPortrait
       || DEVICE_ORIENTATION==UIDeviceOrientationLandscapeLeft
       || DEVICE_ORIENTATION==UIDeviceOrientationLandscapeRight)
        [self positionElements:self.ShowButton];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self positionElements:self.ShowButton];
    
    // Orientation Change Observer and notifer
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:nil];
    
    // I want the navigation controller to roll back and forth; but not visible on the screen.
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    // Gather the Map details
    [self startStandardUpdates];
    // Setup the Hidden view to reveal when page curled
    storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    hiddenVC = [storyboard instantiateViewControllerWithIdentifier:@"HiddenVC"];
    hiddenVC.modalTransitionStyle = UIModalTransitionStylePartialCurl;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)startStandardUpdates
{
    // Create the location manager if this object does not
    // already have one.
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    
    /* Pointout current location with the following accuracy:
    *
    *     kCLLocationAccuracyBestForNavigation  highest + sensor data
    *     kCLLocationAccuracyBest               highest
    *     kCLLocationAccuracyNearestTenMeters   10 meters
    *     kCLLocationAccuracyHundredMeters      100 meters
    *     kCLLocationAccuracyKilometer          1000 meters
    *     kCLLocationAccuracyThreeKilometers    3000 meters
    */
    
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 500; // meters
    
    [locationManager startUpdatingLocation];
}

- (void)startSignificantChangeUpdates
{
    // Create the location manager if this object does not
    // already have one.
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    [locationManager startMonitoringSignificantLocationChanges];
}

// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {

    //Get the current location
    location = [locations lastObject];
    eventDate = location.timestamp;
    
    // If it's a relatively recent event, turn off updates to save power.
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0) {
        // If the event is recent, do something with it.
        // NSLog(@"latitude %+.6f, longitude %+.6f\n",
        //      location.coordinate.latitude,
        //      location.coordinate.longitude);
    }
    
    //hiddenVC.latitude=location.coordinate.latitude;
    //hiddenVC.longitude=location.coordinate.longitude;
    
    [hiddenVC setLatitude:location.coordinate.latitude andLongitude:location.coordinate.longitude];
    
    //Locate it in MAP
    MKCoordinateRegion region;
    region.center.latitude=location.coordinate.latitude;
    region.center.longitude=location.coordinate.longitude;
    region.span.latitudeDelta=0.1;
    region.span.longitudeDelta=0.1;
    [self.wherezMap setRegion:region animated:YES];
    
    //Get the Address details from the coordinates; reverse-Geo-Code
    [self reverseGeocode:location];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        NSLog(@"User has denied location services");
    } else {
        NSLog(@"Location manager FAIL %@",error.description);
    }
}

- (void)reverseGeocode:(CLLocation *)myLocation {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:myLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        //NSLog(@"Finding address");
        if (error) {
            NSLog(@"Error %@", error.description);
        } else {
            CLPlacemark *placemark = [placemarks lastObject];
      
            [hiddenVC setName:placemark.name]; //Name of Business
            [hiddenVC setStreetNo:placemark.subThoroughfare]; //street #
            [hiddenVC setStreetName:placemark.thoroughfare]; //street Name
            [hiddenVC setCity:placemark.locality]; //City
            [hiddenVC setState:placemark.administrativeArea]; //State
            [hiddenVC setZip:placemark.postalCode]; //Zip
            [hiddenVC setCountry:placemark.country]; //Country
            [hiddenVC setCounty:placemark.subAdministrativeArea]; //County
            [hiddenVC setLocality:placemark.subLocality]; //Group name like Bay Area, Puget Sound
    /*
            hiddenVC.debugLabel.textAlignment=NSTextAlignmentLeft;
            hiddenVC.debugLabel.text=[NSString stringWithFormat:@"My Location:\nCoords: %f,%f\n\n%@\n%@ %@\n%@ %@ %@\n%@\n%@ %@\n",
                                  hiddenVC.latitude,hiddenVC.longitude,
                                  hiddenVC.name, //Name of Business
                                  hiddenVC.subThoroughfare, //street #
                                  hiddenVC.thoroughfare, //street Name
                                  hiddenVC.locality, //City
                                  hiddenVC.administrativeArea, //State
                                  hiddenVC.postalCode, //zip
                                  hiddenVC.country, //Country
                                  hiddenVC.subAdministrativeArea, //County
                                  hiddenVC.subLocality]; //Group name like Bay Area, Puget Sound
    */
        }
    }];
}

- (IBAction)showCurl:(id)sender {
    [self presentViewController:hiddenVC animated:YES completion:Nil];

    [self positionElements:hiddenVC.HideButton];
    
    // NSLog(@"PresentingViewCOntroller=[%@]",self.presentingViewController);
    // NSLog(@"PresentedViewCOntroller=[%@]",self.presentedViewController);
    // Presented View is HiddenViewController
}

/************** Open Contacts and get the phone number *******************/
- (IBAction)lookupContact:(id)sender {
    
    //create Picker
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    
    //provide the current object (self/this) as delegate to the picker
    picker.peoplePickerDelegate = (id)self;
    
    //set transision style and show the picker
    picker.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    [self presentViewController:picker animated:YES completion:Nil];
}

// User did not pick any contact
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    // assigning control back to the main controller
	[peoplePicker dismissViewControllerAnimated:YES completion:Nil];
}

// User picks a contact from the contactlist; we grab the details and append into the contact variable
- (BOOL)peoplePickerNavigationController: (ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)pickedContact {
    
	// Picking up firstName, LastName. (Clears "contact" variable)
    if (ABRecordCopyValue(pickedContact, kABPersonFirstNameProperty))
        contact = [NSString stringWithFormat:@"%@,",ABRecordCopyValue(pickedContact, kABPersonFirstNameProperty)];
    if(ABRecordCopyValue(pickedContact, kABPersonLastNameProperty))
       contact = [NSString stringWithFormat:@"%@,",ABRecordCopyValue(pickedContact, kABPersonLastNameProperty)];

    // Picking up all the Phone Numbers with their types (like iphone, mobile, home ...) of the Contact
    ABMultiValueRef phoneNumbers = ABRecordCopyValue(pickedContact,kABPersonPhoneProperty);
    if(phoneNumbers){
        for(CFIndex i=0;i<ABMultiValueGetCount(phoneNumbers);i++)
            contact = [NSString stringWithFormat:@"%@%@:%@,",contact,\
                       ABAddressBookCopyLocalizedLabel(ABMultiValueCopyLabelAtIndex(phoneNumbers,i)),\
                       ABMultiValueCopyValueAtIndex(phoneNumbers, i)];
    }
    // Picking up all the emailIDs
    ABMultiValueRef emailIds = ABRecordCopyValue(pickedContact,kABPersonEmailProperty);
    if(emailIds){
        for(CFIndex i=0;i<ABMultiValueGetCount(emailIds);i++)
            contact = [NSString stringWithFormat:@"%@%@,",contact,ABMultiValueCopyValueAtIndex(emailIds, i)];
    }

    NSLog(@"Contact: %@",contact);
    self.debugContact.text=[NSString stringWithFormat:@"%@",contact];
    
    // Now we have the contact, lets call the API to know wherez the contact
    [ self APIcall];
    
	// Job done. Dismiss Contact List. Go back to the main screen
    [peoplePicker dismissViewControllerAnimated:YES completion:Nil];
    
    return NO;
}


// This method keeps the flow going, after contact is selected in contacts table view.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
    return NO;
}
/************* End Of Contact Picking *************************************/

// This function/method is needed to handle possible special characters in the user contact details
-(NSString *)encodeURL:(NSString *)urlString{
    CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)urlString, NULL, CFSTR("!*'();:@&=+@,/?#[]% "), kCFStringEncodingUTF8);
    return (NSString *)CFBridgingRelease(newString);
}

// API CALL Module
-(void) APIcall{
    //A variable to hold error details, if something errors.
    NSError *apiError = nil;
    
    // Prepare the URL string
    NSString *urlString = [NSString stringWithFormat:@"http://76.103.100.81:3001/?id=%@",[self encodeURL:contact]];
    
    // Define the URL and HTTP methods
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *apiRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    [apiRequest setHTTPMethod:@"GET"];
    
    // DEBUG: Logging Request URL
    // NSLog(@"APIrequest: %@", urlString);
    
    // Call the API
    NSData *apiResponse = [NSURLConnection sendSynchronousRequest:apiRequest returningResponse:nil error:&apiError];
    if (apiError != nil) {
        NSLog(@"API response error: %@", apiError);
        self.debugContact.text=[NSString stringWithFormat:@"Server Error: No response from %@",urlString];
    } // API Call failed
    else{
        // DEBUG: Unicode parsing the data and Logging the response;
        // NSString *apiResponseData = [[NSString alloc] initWithData:apiResponse encoding:NSUTF8StringEncoding];
        // NSLog(@"[SUCCESS]: APIResponse: %@", apiResponseData);
        
        // Parse the JSON response
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:apiResponse options:kNilOptions error:&apiError];
        //NSMutableArray* response = [NSJSONSerialization JSONObjectWithData:apiResponse options:NSJSONReadingMutableContainers error:&apiError];
        if (apiError != nil) {
            NSLog(@"JSON Parse error: %@", apiError);
            self.debugContact.text=[NSString stringWithFormat:@"Server Error: Garbled Response from %@",urlString];
        }
        else{
            NSString *responseID =  [json objectForKey:@"id"];
            NSString *responseMSG = [json objectForKey:@"msg"];
            
            NSLog(@"JSON response : %@ - %@",responseID,responseMSG);
            self.debugContact.text=[NSString stringWithFormat:@"%@ says %@",responseID,responseMSG];
        }
    } // Successful API call
    
}// END OF API CALL Module


-(void)updateDebugLabel:(UITableView *)sender{
/*    UITableViewCell *selectedCell = [sender cellForRowAtIndexPath:indexPath];
    switch(section){
        case ADDRESS_LEVEL_NEAR:
            if(
        case ADDRESS_LEVEL_FAR:
        case ADDRESS_LEVEL_LOCAL:
        case ADDRESS_LEVEL_NONE: return 1; // NO selection
        default: return 0;
    }
 */
}

@end
