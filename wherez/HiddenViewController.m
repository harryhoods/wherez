//
//  HiddenViewController.m
//  wherez
//
//  Created by Hariprasad Prabhakaran on 3/10/14.
//  Copyright (c) 2014 Hariprasad Prabhakaran. All rights reserved.
//

#import "HiddenViewController.h"

@interface HiddenViewController ()

@end

@implementation HiddenViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

//init local static variables;
-(void)setLatitude:(float)lat andLongitude:(float)lng{
    latitude=lat;
    longitude=lng;
}
-(void)setName:(NSString*)str{
    name=str;
}
-(void)setStreetNo:(NSString*)str{
    subThoroughfare=str;
}
-(void)setStreetName:(NSString*)str{
    thoroughfare=str;
}
-(void)setCity:(NSString*)str{
    locality=str;
}
-(void)setState:(NSString*)str{
    administrativeArea=str;
}
-(void)setZip:(NSString*)str{
    postalCode=str;
}
-(void)setCountry:(NSString*)str{
    country=str;
}
-(void)setCounty:(NSString*)str{
    subAdministrativeArea=str;
}
-(void)setLocality:(NSString*)str{
    subLocality=str;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
/*
    UILabel *sectionHeader = [[UILabel alloc] initWithFrame:CGRectNull];
    sectionHeader.backgroundColor = [UIColor clearColor];
    sectionHeader.textAlignment = NSTextAlignmentLeft;
    sectionHeader.font = [UIFont systemFontOfSize:12];
    sectionHeader.textColor = [UIColor whiteColor];
    sectionHeader.frame = CGRectMake(0, 0, self.tableView.bounds.size.width, 30);
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSBundle *bundle = [NSBundle mainBundle];
    NSDictionary *locinfo = [bundle localizedInfoDictionary];
    NSString *productName =  [locinfo objectForKey:@"CFBundleDisplayName"];
    sectionHeader.text = [productName stringByAppendingString:version];
    
    self.tableView.tableFooterView = sectionHeader;
 */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

NS_ENUM(NSUInteger, eADDRESS_LEVEL){
    ADDRESS_LEVEL_NEAR,
    ADDRESS_LEVEL_FAR,
    ADDRESS_LEVEL_LOCAL,
    ADDRESS_LEVEL_NONE,
    ADDRESS_LEVEL_COUNT
};
NS_ENUM(NSUInteger, eADDRESS_LEVEL_FAR){
    STREET_NO,
    STREET_NAME,
    CITY,
    STATE,
    ZIP,
    COUNTRY,
    ADDRESS_LEVEL_FAR_COUNT
};
NS_ENUM(NSUInteger, eADDRESS_LEVEL_LOCAL){
    COUNTY,
    LOCALITY,
    ADDRESS_LEVEL_LOCAL_COUNT
};

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return ADDRESS_LEVEL_COUNT;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch(section){
        case ADDRESS_LEVEL_NEAR: return 1; // Co-ordinates (x,y)
        case ADDRESS_LEVEL_FAR: return ADDRESS_LEVEL_FAR_COUNT;
        case ADDRESS_LEVEL_LOCAL: return ADDRESS_LEVEL_LOCAL_COUNT;
        case ADDRESS_LEVEL_COUNT: return ADDRESS_LEVEL_COUNT;
        case ADDRESS_LEVEL_NONE: return 1; // NO selection
        default: return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch(section){
        case ADDRESS_LEVEL_NEAR: return 80; // space inset on TOP
        case ADDRESS_LEVEL_FAR: return 20; // space inset on TOP
        default:
            break;
    }
    return 5; // sepration between other sections
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

#pragma mark - Table view delegate
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 50)];
    UILabel *sectionHeader = [[UILabel alloc] initWithFrame:CGRectMake(12, -20, tableView.bounds.size.width, 50)];
    sectionHeader.backgroundColor = [UIColor clearColor];
    sectionHeader.textAlignment = NSTextAlignmentLeft;
    sectionHeader.font = [UIFont systemFontOfSize:14];
    sectionHeader.textColor = [UIColor blackColor];
    [sectionView addSubview:sectionHeader];

    switch (section) {
        case ADDRESS_LEVEL_NEAR:
            // This section displays the option to choose the coordinates (Latitude and Longitude)
            //sectionHeader.text = NSLocalizedString(@"Your friends can your exact location:", @"Share Location Details");
            break;
        case ADDRESS_LEVEL_FAR:
            sectionHeader.text = NSLocalizedString(@"Your friends can only see the selected detail:", @"Share Location Details");
            break;
        case ADDRESS_LEVEL_LOCAL:
            // Local level address choices like county, locality (bay area, puget sound, etc.)
            //sectionHeader.text = NSLocalizedString(@"These are local labels:", @"Share Location Details");
            break;
        case ADDRESS_LEVEL_NONE:
            // Hide All
            //sectionHeader.text = NSLocalizedString(@"Go incognito to your wherez friends:", @"Share Location Details");
            break;
        default:
            break;
    }
    return sectionView;
    
}

// Create initial view of the settings Menu
- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addressCells"];

    [cell.textLabel setTextColor:[UIColor darkGrayColor]];
    cell.textLabel.font = [UIFont systemFontOfSize:17];
    cell.textLabel.backgroundColor=[UIColor clearColor]; // Nullifying any unintended settings
    [cell setSelected:NO]; //Default unselected
    
    switch (indexPath.section) {
        case ADDRESS_LEVEL_NEAR:
            // Share Coordinates
            cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"Coordinates"), NSLocalizedString(@"Coordinates", "Map Coordinates")];
            break;
        case ADDRESS_LEVEL_FAR:
            // Share the User Choice from "street # to Country"
            switch (indexPath.row) {
                case STREET_NO:{
                    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@",@"Street Address"), NSLocalizedString(@"Street Address", nil)];
                    break;
                }
                case STREET_NAME:
                    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"Street Name Only"), NSLocalizedString(@"Street Name Only", nil)];
                    break;
                case CITY:
                    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"CITY and State"), NSLocalizedString(@"City and State", "City")];
                    [cell setSelected:YES];
                    finalUserChoice=[NSString stringWithFormat:@"%@,%@",locality,administrativeArea];
                    self.debugLabel.text=finalUserChoice;
                    break;
                case STATE:
                    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"State Only"), NSLocalizedString(@"State Only", "Region")];
                    break;
                case ZIP:
                    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"Zip Code"), NSLocalizedString(@"Zip Code", "Postal Code")];
                    break;
                case COUNTRY:
                    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"Country"), NSLocalizedString(@"Country", "Nation")];
                    break;
                default:
                    break;
            }
            break;
        case ADDRESS_LEVEL_LOCAL:
            switch (indexPath.row) {
                case COUNTY:
                    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"County"), NSLocalizedString(@"County", "District")];
                    break;
                case LOCALITY:
                    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"Locality"), NSLocalizedString(@"Locality", "Local Region")];
                    break;
                default:
                    break;
            }
            break;
        case ADDRESS_LEVEL_NONE:
            // Share Coordinates
            cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"None"), NSLocalizedString(@"None", "Hide")];
            break;
        default:
            break;
    }
    
    if(cell.selected)
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType=UITableViewCellAccessoryNone;
    return cell;
}

// When user interacts with the settings Menu
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSIndexPath *toggleCellIndex = nil;
    UITableViewCell *tempCell = nil;
    finalUserChoice = nil;

    //If selection is not coordinate, deselct coordinate
    if(indexPath.section!=ADDRESS_LEVEL_NEAR){
        toggleCellIndex = [NSIndexPath indexPathForRow:0 inSection:ADDRESS_LEVEL_NEAR];
        tempCell = [tableView cellForRowAtIndexPath:toggleCellIndex];
        [tableView deselectRowAtIndexPath:toggleCellIndex animated:YES];
        [tempCell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    //If selection is NONE, deselct All
    if(indexPath.section==ADDRESS_LEVEL_NONE){
        finalUserChoice=[NSString stringWithFormat:@"%@", @"Not Sharing Location"];
        [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
        //Delselect the coordinates
        toggleCellIndex = [NSIndexPath indexPathForRow:0 inSection:ADDRESS_LEVEL_NEAR];
        tempCell = [tableView cellForRowAtIndexPath:toggleCellIndex];
        [tableView deselectRowAtIndexPath:toggleCellIndex animated:YES];
        [tempCell setAccessoryType:UITableViewCellAccessoryNone];
        //Deselect all FAR selections
        for(int i=0;i<ADDRESS_LEVEL_FAR_COUNT;i++){
            toggleCellIndex = [NSIndexPath indexPathForRow:i inSection:ADDRESS_LEVEL_FAR];
            tempCell = [tableView cellForRowAtIndexPath:toggleCellIndex];
            [tableView deselectRowAtIndexPath:toggleCellIndex animated:YES];
            [tempCell setAccessoryType:UITableViewCellAccessoryNone];
        }
        //Deselect all LOCAL selections
        for(int i=0;i<ADDRESS_LEVEL_LOCAL_COUNT;i++){
            toggleCellIndex = [NSIndexPath indexPathForRow:i inSection:ADDRESS_LEVEL_LOCAL];
            tempCell = [tableView cellForRowAtIndexPath:toggleCellIndex];
            [tableView deselectRowAtIndexPath:toggleCellIndex animated:YES];
            [tempCell setAccessoryType:UITableViewCellAccessoryNone];
        }
    }
    else{
        // Just for checkmark Animation and inter-dependent deselections
        switch (indexPath.section) {
            case ADDRESS_LEVEL_NEAR:
                finalUserChoice=[NSString stringWithFormat:@"latitude:%f,longitude:%f",latitude,longitude];
                NSLog(@"Display the Coordinates;");
                //Deselect all FAR selections
                for(int i=0;i<ADDRESS_LEVEL_FAR_COUNT;i++){
                    toggleCellIndex = [NSIndexPath indexPathForRow:i inSection:ADDRESS_LEVEL_FAR];
                    [tableView deselectRowAtIndexPath:toggleCellIndex animated:YES];
                    [[tableView cellForRowAtIndexPath:toggleCellIndex] setAccessoryType:UITableViewCellAccessoryNone];
                }
                //Deselect all LOCAL selections
                for(int i=0;i<ADDRESS_LEVEL_LOCAL_COUNT;i++){
                    toggleCellIndex = [NSIndexPath indexPathForRow:i inSection:ADDRESS_LEVEL_LOCAL];
                [tableView deselectRowAtIndexPath:toggleCellIndex animated:YES];
                [[tableView cellForRowAtIndexPath:toggleCellIndex] setAccessoryType:UITableViewCellAccessoryNone];
                }
                break;
                case ADDRESS_LEVEL_FAR:
                    switch (indexPath.row) {
                        case STREET_NO:
                            //set street NO
                            // if steet number is selected, unselect steet name only option and vice versa.
                            if(selectedCell.accessoryType==UITableViewCellAccessoryNone){
                                toggleCellIndex = [NSIndexPath indexPathForRow:STREET_NAME inSection:ADDRESS_LEVEL_FAR];
                                [tableView deselectRowAtIndexPath:toggleCellIndex animated:YES];
                                [[tableView cellForRowAtIndexPath:toggleCellIndex] setAccessoryType:UITableViewCellAccessoryNone];
                            }
                            break;
                        case STREET_NAME:
                            //set street name
                            // if steet number is selected, unselect steet name only option and vice versa.
                            if(selectedCell.accessoryType==UITableViewCellAccessoryNone){
                                toggleCellIndex = [NSIndexPath indexPathForRow:STREET_NO inSection:ADDRESS_LEVEL_FAR];
                                [tableView deselectRowAtIndexPath:toggleCellIndex animated:YES];
                                [[tableView cellForRowAtIndexPath:toggleCellIndex] setAccessoryType:UITableViewCellAccessoryNone];
                            }
                            break;
                        case CITY:
                            //set city and state
                            // if steet number is selected, unselect steet name only option and vice versa.
                            if(selectedCell.accessoryType==UITableViewCellAccessoryNone){
                                toggleCellIndex = [NSIndexPath indexPathForRow:STATE inSection:ADDRESS_LEVEL_FAR];
                                [tableView deselectRowAtIndexPath:toggleCellIndex animated:YES];
                                [[tableView cellForRowAtIndexPath:toggleCellIndex] setAccessoryType:UITableViewCellAccessoryNone];
                            }
                            break;
                        case STATE:
                            //set state only
                            // if steet number is selected, unselect steet name only option and vice versa.
                            if(selectedCell.accessoryType==UITableViewCellAccessoryNone){
                                toggleCellIndex = [NSIndexPath indexPathForRow:CITY inSection:ADDRESS_LEVEL_FAR];
                                [tableView deselectRowAtIndexPath:toggleCellIndex animated:YES];
                                [[tableView cellForRowAtIndexPath:toggleCellIndex] setAccessoryType:UITableViewCellAccessoryNone];
                            }
                            break;
                        case ZIP:
                            //set zip
                            break;
                        case COUNTRY:
                            //set COUNTRY
                            break;
                            /*                case TEST:{
                             NSString *iTunesLink = @"https://itunes.apple.com/us/artist/poker-chang-ltd/id312276632";
                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                             break;
                             }
                             */
                        default:
                            break;
                    }
                break;
            case ADDRESS_LEVEL_LOCAL:
                switch (indexPath.row) {
                    case COUNTY:
                        //set COUNTY
                        break;
                    case LOCALITY:
                        //set Locality
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
    
        if(selectedCell.accessoryType==UITableViewCellAccessoryCheckmark){
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [selectedCell setAccessoryType:UITableViewCellAccessoryNone];
        }
        else{
            [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
    
/*        //Update User Selection for ADDRESS_LEVEL_FAR and ADDRESS_LEVEL_LOCAL
        //NSMutableArray *addressLevelFar = [NSMutableArray arrayWithObjects:self.subThoroughfare,self.thoroughfare,self.locality,self.administrativeArea,self.postalCode,self.country,nil];
        //NSArray * addressLevelLocal = [NSArray arrayWithObjects:self.subAdministrativeArea,self.subLocality,nil];
        //NSMutableArray *addressLevelLocal = [NSMutableArray arrayWithObjects:@"test",@"rest",nil];
        
        NSLog(@"AddressFar:[%d]",[addressLevelFar count]);
        for(int i=0;i<ADDRESS_LEVEL_LOCAL_COUNT;i++){
            if([addressLevelLocal count])
                NSLog(@"addressLocal[%d] = %@",i,addressLevelLocal[i]);
        }
*/
        //Iterate all FAR selections
        for(int i=0;i<ADDRESS_LEVEL_FAR_COUNT;i++){
            toggleCellIndex = [NSIndexPath indexPathForRow:i inSection:ADDRESS_LEVEL_FAR];
            tempCell = [tableView cellForRowAtIndexPath:toggleCellIndex];
            if(tempCell.accessoryType==UITableViewCellAccessoryCheckmark){
                switch (i) {
                    case STREET_NO:
                        finalUserChoice=[NSString stringWithFormat:@"%@ %@",subThoroughfare,thoroughfare];
                        break;
                    case STREET_NAME:
                        finalUserChoice=[NSString stringWithFormat:@"%@",thoroughfare];
                        break;
                    case CITY:
                        if(finalUserChoice)
                            finalUserChoice=[NSString stringWithFormat:@"%@,%@,%@",finalUserChoice,locality,administrativeArea];
                        else
                            finalUserChoice=[NSString stringWithFormat:@"%@,%@",locality,administrativeArea];
                        break;
                    case STATE:
                        if(finalUserChoice)
                            finalUserChoice=[NSString stringWithFormat:@"%@,%@",finalUserChoice,administrativeArea];
                        else
                            finalUserChoice=[NSString stringWithFormat:@"%@",administrativeArea];
                        break;
                    case ZIP:
                        if(finalUserChoice)
                            finalUserChoice=[NSString stringWithFormat:@"%@,%@",finalUserChoice,postalCode];
                        else
                            finalUserChoice=[NSString stringWithFormat:@"%@",postalCode];
                        break;
                    case COUNTRY:
                        if(finalUserChoice)
                            finalUserChoice=[NSString stringWithFormat:@"%@,%@ county",finalUserChoice,country];
                        else
                            finalUserChoice=[NSString stringWithFormat:@"%@ county",country];
                        break;
                    default:
                        //NSString *iTunesLink = @"https://itunes.apple.com/us/artist/poker-chang-ltd/id312276632";
                        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                        break;
                }
            }
        }
        //Iterate all LOCAL selections
        for(int i=0;i<ADDRESS_LEVEL_LOCAL_COUNT;i++){
            toggleCellIndex = [NSIndexPath indexPathForRow:i inSection:ADDRESS_LEVEL_LOCAL];
            tempCell = [tableView cellForRowAtIndexPath:toggleCellIndex];
            if(tempCell.accessoryType==UITableViewCellAccessoryCheckmark){
                switch (i) {
                    case COUNTY:
                        if(finalUserChoice)
                            finalUserChoice=[NSString stringWithFormat:@"%@,%@",finalUserChoice,subAdministrativeArea];
                        else
                            finalUserChoice=[NSString stringWithFormat:@"%@",subAdministrativeArea];
                        break;
                    case LOCALITY:
                        if(finalUserChoice)
                            finalUserChoice=[NSString stringWithFormat:@"%@,%@",finalUserChoice,subLocality];
                        else
                            finalUserChoice=[NSString stringWithFormat:@"%@",subLocality];
                        break;
                    default:
                        break;
                }
            }
        }

    } // END of else (selection is not None)
    
    //NSLog(@"addressLevelLocal[0]:%@, addressLevelLocal[1]:%@",addressLevelLocal[0],addressLevelLocal[1]);
    NSLog(@"finalUserChoice:%@",finalUserChoice);
    self.debugLabel.text=finalUserChoice;
}


/**************************************************************/
- (IBAction)dismissView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (IBAction)updateSettings:(id)sender {
    if (![sender isSelected]){
        [sender setBackgroundColor:[UIColor greenColor]];
        [sender setSelected:YES];
    }
    else{
        [sender setBackgroundColor:Nil];
        [sender setSelected:NO];
    }
/**************************************************************/
}

@end
