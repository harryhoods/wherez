- DEMO: https://youtu.be/xSqV3UJbFuw

- git clone/pull wherez from bitbucket 
- Install nodejs v0.10.26 from http://nodejs.org/download/
- Start the server 
    - node <git clone path>/wherez-server/app.js for example, node /Volumes/BI/wherez/wherez-server/app.js
- Hit the end point by providing an id
    - http://localhost:3001/?id=abc to get response {"id": "abc", "msg": "I am at Hawai!!!"}

- Install mongoDB 2.4.10 
    - http://www.mongodb.org/downloads 
    - For OSX 
        - cd to the directory where you want it to be installed 
        - curl -O http://downloads.mongodb.org/osx/mongodb-osx-x86_64-2.4.10.tgz 
        - tar -zxvf mongodb-osx-x86_64-2.4.10.tgz
        - mkdir -p mongodb-osx-x86_64-2.4.10/data
        - chown `id -u` mongodb-osx-x86_64-2.4.10/data
        - mongodb-osx-x86_64-2.4.10/bin/mongod --dbpath `pwd`/mongodb-osx-x86_64-2.4.10/data



